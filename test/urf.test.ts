import { monthly_urf } from '../src';

describe('monthly_urf', () => {
  it('should have a value in the urf as 0.03286', () => {
    expect(monthly_urf(5000, 100000, 224430, 0.2)).toContain(
      0.03286222193165302
    );
  });

  it('same test without specific sy variable', () => {
    expect(monthly_urf(5000, 100000, 224430)).toContain(0.03286222193165302);
  });

  it('should add up to 1 or very close', () => {
    const reducer: any = (accumulator: number, currentValue: number) =>
      accumulator + currentValue;
    const total_value: number = monthly_urf(5000, 100000, 224430, 0.2).reduce(
      reducer
    );

    expect(total_value).toBeCloseTo(1);
  });
});
