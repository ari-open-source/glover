import { sqrt, erf, pi, exp, abs, square } from 'mathjs';

const pumping_percent_depletion_volume = (
  // percent in days
  dist_to_stream: number,
  aq_const: number,
  timeSince: number
): number => {
  if (timeSince <= 0) {
    return 0;
  }
  // console.log('timeSince: ', timeSince);
  // console.log('AQ_Const: ', aq_const);

  const ui = dist_to_stream / sqrt(4 * aq_const * timeSince);
  // console.log('ui: ', ui);
  // console.log('erf(ui)', erf(ui));

  const term1 = (2 * square(ui) + 1) * (1 - erf(ui));
  // console.log('term1: ', term1);

  let term2: number;
  if (ui > 15) {
    term2 = 0;
  } else {
    term2 = (2 * ui) / sqrt(pi) / exp(square(ui));
  }

  // console.log('term2: ', term2);
  return term1 - term2;
};

const bounded_pumping_percent_dep_vol = (
  dist_to_stream: number,
  no_flow_dist: number,
  aq_const: number,
  timeSince: number
) => {
  if (timeSince <= 0) {
    return 0;
  }

  // console.log('Time Since: ', timeSince);

  let term: number = pumping_percent_depletion_volume(
    dist_to_stream,
    aq_const,
    timeSince
  );
  let sum: number = term;
  let sign: number = 1;
  let mult: number = 2;

  do {
    term = pumping_percent_depletion_volume(
      mult * no_flow_dist - dist_to_stream,
      aq_const,
      timeSince
    );
    sum = sum + sign * term;
    term = pumping_percent_depletion_volume(
      mult * no_flow_dist + dist_to_stream,
      aq_const,
      timeSince
    );
    sum = sum - sign * term;
    mult = mult + 2;
    sign = 0 - sign;
  } while (abs(term) > 1e-8);

  return sum;
};

const aquifer_const = (trans: number, sy: number): number => {
  // trans is a transmissivity in gpd/ft and sy is specific yield usually 0.2
  // examples are in ft2/day 30000 ft2/day = 224,430
  return trans / 7.481 / sy;
};

// example test (5000, 100000, 224430, 0.2)
export const monthly_urf = (
  well_dist: number,
  no_flow_dist: number,
  trans: number,
  sy: number = 0.2
): number[] => {
  let results: number[] = [];
  let result: number = 0;
  let step: number = 30.5;
  let pre_result: number = 0;
  do {
    pre_result = result;
    result = bounded_pumping_percent_dep_vol(
      well_dist,
      no_flow_dist,
      aquifer_const(trans, sy),
      step
    );
    results.push(result);
    step = step + 30.5;

    if (results.length > 800) {
      break;
    }
  } while (abs(result - pre_result) > 0.00001);

  // console.log('results: ', results);
  let remainder: number = 0;
  const max_val = results[results.length - 1];
  if (max_val > 0.0001) {
    // distribute back to the other elements
    // value to distribute
    remainder = 1 - max_val;
  }
  // console.log('remainder: ', remainder);
  let stepresults: number[] = [];
  for (let i = 0; i < results.length; i++) {
    let rs: number = 0;
    if (i === 0) {
      rs = results[i] + (results[i] / max_val) * remainder;
    } else {
      if (results[i + 1]) {
        // console.log(results[i], max_val, remainder, (results[i + 1] - results[i]) / max_val * remainder);
        rs =
          results[i] -
          results[i - 1] +
          ((results[i] - results[i - 1]) / max_val) * remainder;
      }
    }

    // console.log('i:', i, 'result:', results[i], 'rs: ', rs);
    stepresults.push(rs);
  }

  const reducer: any = (accumulator: number, currentValue: number) =>
    accumulator + currentValue;
  const total_value: number = stepresults.reduce(reducer);

  if (total_value < 0.99) {
    // throw new Error(
    //   `Curve Error, not normalized, max value: ${max_val}, total returned: ${total_value}`
    // );
    console.log(
      `WARNING: Curve Error, not normalized, max value: ${max_val}, total returned: ${total_value}`
    );
  }

  return stepresults;
};
