import { sqrt, erf, pi, exp } from 'mathjs';
import moment from 'moment';
export var sum = function (a, b) {
    if ('development' === process.env.NODE_ENV) {
        console.log('boop');
    }
    return a + b;
};
// const pumping_percent_depletion_rate = (
//   dist_to_stream: number,
//   aq_const: number,
//   timeSince: number
// ): number => {
//   if (timeSince <= 0) {
//     return 0;
//   }
//   const ui = dist_to_stream / sqrt(4 * aq_const * timeSince);
//   return 1 - erf(ui);
// };
var pumping_percent_depletion_volume = function (dist_to_stream, aq_const, timeSince) {
    if (timeSince <= 0) {
        return 0;
    }
    var ui = dist_to_stream / sqrt(4 * aq_const * timeSince);
    var term1 = (2 * ui * ui + 1) * (1 - erf(ui));
    var term2;
    if (ui > 15) {
        term2 = 0;
    }
    else {
        term2 = (2 * ui) / sqrt(pi) / exp(ui * ui);
    }
    return term1 - term2;
};
var bounded_pumping_percent_dep_vol = function (dist_to_stream, no_flow_dist, aq_const, timeSince) {
    if (timeSince <= 0) {
        return 0;
    }
    var term = pumping_percent_depletion_volume(dist_to_stream, aq_const, timeSince);
    var sum = term;
    var sign = 1;
    var mult = 2;
    do {
        term = pumping_percent_depletion_volume(mult * no_flow_dist - dist_to_stream, aq_const, timeSince);
        sum = sum + sign * term;
        term = pumping_percent_depletion_volume(mult * no_flow_dist + dist_to_stream, aq_const, timeSince);
        sum = sum - sign * term;
        mult = mult + 2;
        sign = 0 - sign;
    } while (term > 1e-8);
    return sum;
};
var aquifer_const = function (trans, sy) {
    // trans is a transmissivity in gpd/ft and sy is specific yield usually 0.2
    return trans / 7.48 / sy / 24 / 3600;
};
export var glover_array_monthly = function (dist_to_stream, noflow_bound, trans, sy, p, start_yr, YearType, t) {
    var conv = 24 * 3600;
    var s1;
    var s2;
    var total = 0;
    var cum_tp = 0;
    var currDate;
    var nextDate;
    var tpi;
    switch (YearType) {
        case 1:
            currDate = moment().set({ year: start_yr - 1, month: 11, date: 1 });
            break;
        case 2:
            currDate = moment().set({ year: start_yr - 1, month: 10, date: 1 });
            break;
        default:
            currDate = moment().set({ year: start_yr, month: 1, date: 1 });
    }
    var aq_const = aquifer_const(trans, sy);
    s2 = bounded_pumping_percent_dep_vol(dist_to_stream, noflow_bound, aq_const, (t - cum_tp) * conv * (t - cum_tp));
    p.forEach(function (row) {
        nextDate = moment(currDate).add(1, 'M');
        tpi = nextDate.diff(currDate, 'months');
        if (cum_tp <= t) {
            s1 = s2;
            s2 = bounded_pumping_percent_dep_vol(dist_to_stream, noflow_bound, aq_const, (t - tpi - cum_tp) * conv);
            s2 = s2 * (t - tpi - cum_tp);
            total = total + ((s1 - s2) / tpi) * row;
        }
        cum_tp = cum_tp + tpi;
        currDate = nextDate;
    });
    return total;
};
//# sourceMappingURL=index.js.map