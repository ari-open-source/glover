# Glover Calculator

This is a calculator for the "Glover" solution
to ground water depletion calculations required for lagging of depletion between the stream and 
aquifer. This module allows for the entry of the glover parameters and returns a monthly Unit Response Function (URF) 
that then can be used to distribute well pumping for stream depletion analysis. 

## Input and entry
To use this import the module `const glover = require('.dist/index.js')` or in ES6 `import { monthly_urf } from 'glover'`and then call the main entry point 
of the function `glover.monthly_urf(well_dist, no_flow_dist, transmissivity, [sy])` or ES6 `monthly_urf(well_dist, no_flow_dist, transmissivity, [sy])` and this returns
an array of monthly factors of depletion.

### Variables and units
- well_dist: distance from well to stream (ft)
- no_flow_dist: distance from the no flow boundary through the well to the stream (ft)
- transmissivity: transmissivity of the aquifer in GDP/ft
- sy: optional specific yield of the aquifer (defaults to 0.2)

### Return
The return is a monthly array of the parameters that is either until the difference between the monthly 
values is `< 0.00001` or is 600 months (50 years). If the 600 months mark is the limit, then the curve is normalized
to 1 based upon a weighted distribution to each of the factors throughout the curve. This was created
to ensure no water is lost during lagging.

## Development
This project was bootstrapped with [TSDX](https://github.com/jaredpalmer/tsdx). 

### Local Development

Below is a list of commands you will probably find useful.

#### `npm start` or `yarn start`

Runs the project in development/watch mode. Your project will be rebuilt upon changes. TSDX has a special logger for you convenience. Error messages are pretty printed and formatted for compatibility VS Code's Problems tab.

#### `npm run build` or `yarn build`

Bundles the package to the `dist` folder.
The package is optimized and bundled with Rollup into multiple formats (CommonJS, UMD, and ES Module).

#### `npm test` or `yarn test`

Runs the test watcher (Jest) in an interactive mode.
By default, runs tests related to files changed since the last commit.
